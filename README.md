# ChatMessage #

iOS client showing a chat message parser app.

### Prerequisites ###

* [Git](http://git-scm.com/)
* [Xcode](https://developer.apple.com/xcode/)
* [CocoaPods](http://cocoapods.org/)

## Installation

1. Clone the repo

2. Pods are already pushed themselves in the repo, but you can run `pod update/install` to re-install. Remember to use .xcworkspace.

3. Run unit tests -> Command + U

4. Run the app -> Command + R