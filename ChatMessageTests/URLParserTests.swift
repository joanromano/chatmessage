//
//  URLParserTests.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest

class URLParserTests: XCTestCase {
    
    func testURLOnlyLinks() {
        let sut = URLParser()
        let result = sut.parse("https://www.nbcolympics.com")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "https://www.nbcolympics.com")
    }
    
    func testURLLinksWithText() {
        let sut = URLParser()
        let result = sut.parse("Olympics are starting soon;https://www.nbcolympics.com")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "https://www.nbcolympics.com")
    }
    
    func testURLLinksMissing() {
        let sut = URLParser()
        let result = sut.parse("Olympics are starting soon")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testURLLinksEmptyString() {
        let sut = URLParser()
        let result = sut.parse("")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testMutipleURLs() {
        let sut = URLParser()
        let result = sut.parse("Olympics are starting soon; https://www.nbcolympics.com asdsads https://google.com")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "https://www.nbcolympics.com")
        XCTAssertEqual(result.last as! String, "https://google.com")
    }
    
    func testURLTitle() {
        let sut = URLTitleParser()
        let result = sut.parse("<html><head><title>A title</title></head><body>The content of the document......</body></html>")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "A title")
    }
    
    func testURLTitleNoResults() {
        let sut = URLTitleParser()
        let result = sut.parse("<html><body><h1>My First Heading</h1><p>My first paragraph.</p></body></html>")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testURLTitleMissingClosingTag() {
        let sut = URLTitleParser()
        let result = sut.parse("<html><head><title>A title</head><body>The content of the document......</body></html>")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testURLTitleMissingOpeningTag() {
        let sut = URLTitleParser()
        let result = sut.parse("<html><head>A title</title></head><body>The content of the document......</body></html>")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testURLTitleemptyString() {
        let sut = URLTitleParser()
        let result = sut.parse("")
        
        XCTAssertEqual(result.count, 0)
    }
}
