//
//  NonAlphanumericParserTests.swift
//  ChatMessageTests
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest
@testable import ChatMessage

class NonAlphanumericParserTests: XCTestCase {
    
    func testNonAlphanumericWithEmoticons() {
        let sut = NonAlphanumericParser(EmoticonParser())
        let result = sut.parse("(hola) sadsada (adios) sadasd")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "hola")
        XCTAssertEqual(result.last as! String, "adios")
    }
    
    func testNonAlphanumericWithEmoticonsNoMatches() {
        let sut = NonAlphanumericParser(EmoticonParser())
        let result = sut.parse("hola @aaasda adios")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testNonAlphanumericWithMentions() {
        let sut = NonAlphanumericParser(MentionParser())
        let result = sut.parse("@hola sadsada @adios sadasd")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "hola")
        XCTAssertEqual(result.last as! String, "adios")
    }
    
    func testNonAlphanumericWithMentionsNoMatches() {
        let sut = NonAlphanumericParser(MentionParser())
        let result = sut.parse("hola aaasda (adios)")
        
        XCTAssertEqual(result.count, 0)
    }
}
