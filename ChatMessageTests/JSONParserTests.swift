//
//  ParserTests.swift
//  ChatMessage
//
//  Created by Joan Romano on 07/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest

import SwiftyJSON

class ParserTests: XCTestCase {
    
    func testParserWithMentions() {
        let sut = JSONParser(parsers: [NonAlphanumericParser(MentionParser())])
        let json = JSON(sut.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"))
        
        XCTAssertTrue(json["mentions"].exists())
        XCTAssertFalse(json["emoticons"].exists())
        XCTAssertFalse(json["links"].exists())
        XCTAssertEqual(json.count, 1)
        XCTAssertEqual(json["mentions"].count, 2)
        XCTAssertEqual(json["mentions"][0], "bob")
        XCTAssertEqual(json["mentions"][1], "john")
    }
    
    func testParserWithMentionsNoResults() {
        let sut = JSONParser(parsers: [NonAlphanumericParser(MentionParser())])
        let json = JSON(sut.parse("such a cool feature;"))
        
        XCTAssertFalse(json["mentions"].exists())
        XCTAssertEqual(json.count, 0)
    }
    
    func testParserWithEmoticons() {
        let sut = JSONParser(parsers: [NonAlphanumericParser(EmoticonParser())])
        let json = JSON(sut.parse("@bob @john (success) (yay) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"))
        
        XCTAssertFalse(json["mentions"].exists())
        XCTAssertTrue(json["emoticons"].exists())
        XCTAssertFalse(json["links"].exists())
        XCTAssertEqual(json.count, 1)
        XCTAssertEqual(json["emoticons"].count, 2)
        XCTAssertEqual(json["emoticons"][0], "success")
        XCTAssertEqual(json["emoticons"][1], "yay")
    }
    
    func testParserWithMentionsAndEmoticons() {
        let sut = JSONParser(parsers: [NonAlphanumericParser(MentionParser()), NonAlphanumericParser(EmoticonParser())])
        let json = JSON(sut.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"))
        
        XCTAssertTrue(json["mentions"].exists())
        XCTAssertTrue(json["emoticons"].exists())
        XCTAssertEqual(json.count, 2)
        XCTAssertEqual(json["mentions"].count, 2)
        XCTAssertEqual(json["emoticons"].count, 1)
        XCTAssertEqual(json["emoticons"][0], "success")
    }
    
    func testParserWithMentionsEmoticonsAndURL() {
        let sut = JSONParser(parsers: [NonAlphanumericParser(MentionParser()), NonAlphanumericParser(EmoticonParser()), URLParser()])
        let json = JSON(sut.parse("@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016"))
        
        XCTAssertTrue(json["mentions"].exists())
        XCTAssertTrue(json["emoticons"].exists())
        XCTAssertTrue(json["url"].exists())
        XCTAssertEqual(json.count, 3)
        XCTAssertEqual(json["mentions"].count, 2)
        XCTAssertEqual(json["emoticons"].count, 1)
        XCTAssertEqual(json["url"].count, 1)
        XCTAssertEqual(json["url"][0], "https://twitter.com/jdorfman/status/430511497475670016")
    }
    
    private func JSON(_ parsed: String) -> JSON {
        return SwiftyJSON.JSON(data: parsed.data(using: .utf8, allowLossyConversion: false)!)
    }
}
