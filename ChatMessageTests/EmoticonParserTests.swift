//
//  EmoticonParserTests.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest

class EmoticonParserTests: XCTestCase {
    
    func testParseOnlyEmoticons() {
        let sut = EmoticonParser()
        let result = sut.parse("(hola) (adios)")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "(hola)")
        XCTAssertEqual(result.last as! String, "(adios)")
    }
    
    func testParseEmoticonsWithText() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (hola) asdasdasd (adios) (venga)")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "(hola)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testParseNoEmoticons() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd @hola asdasdasd aasdasd adios")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testParseEmptyText() {
        let sut = EmoticonParser()
        let result = sut.parse("")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testEmptyEmoticons() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd () asdasdasd (adios) (venga)")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "(adios)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testNotLongerThan15CharEmoticons() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd () asdasdasd (adiosadiosadiosadiosadiosadios) (venga)")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "(venga)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testExactly15CharEmoticons() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd () asdasdasd (adiosadiosadios) (venga)")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "(adiosadiosadios)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testEmoticonsWithNumbers() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (party123) asdasdasd (adios) (venga)")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "(party123)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testNonAlphanumericInvalidEmoticon() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (party123) asdasdasd (something#@) (%invalid) (&) (adios) (venga)")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "(party123)")
        XCTAssertEqual(result.last as! String, "(venga)")
    }
    
    func testOnlyInvalidEmoticon() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (something#@) (%invalid) (&) (/byebye) (hola!)")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testEmoticonInsideEmoticon() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (hello(world)) (hola) moretextx here")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "(world)")
        XCTAssertEqual(result.last as! String, "(hola)")
    }
    
    func testTwoEmoticonInsideEmoticon() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (hello(world(alone))) (hola) moretextx here")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "(alone)")
        XCTAssertEqual(result.last as! String, "(hola)")
    }
    
    func testMultipleParenthesis() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (((what))hola) ((adios)) ((hello(alone))) moretextx here")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "(what)")
        XCTAssertEqual(result[1] as! String, "(adios)")
        XCTAssertEqual(result.last as! String, "(alone)")
    }
    
    func testEmoticonInsideEmoticonTooLong() {
        let sut = EmoticonParser()
        let result = sut.parse("asdasd (hello(worldworldworldworld)) (hola) moretextx here")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "(hola)")
        XCTAssertEqual(result.last as! String, "(hola)")
    }
    
}
