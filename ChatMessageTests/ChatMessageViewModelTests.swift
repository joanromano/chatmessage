//
//  ChatMessageViewModelTests.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest

class ChatMessageViewModelTests: XCTestCase {
    
    func testDefaultValues() {
        let sut = ChatMessageViewModel(interactor: ChatMessageFakeInteractor())
        
        XCTAssertEqual(sut.parsedInput(), "No results")
    }
    
    func testValuesAfterParse() {
        let sut = ChatMessageViewModel(interactor: ChatMessageFakeInteractor())
        sut.parseInput("some input")
        
        XCTAssertTrue(sut.parsedInput().contains("foo"))
        XCTAssertTrue(sut.parsedInput().contains("bar"))
    }
    
    func testValuesAfterEmptyParse() {
        let interactor = ChatMessageFakeInteractor()
        interactor.emptyParse = true
        let sut = ChatMessageViewModel(interactor: interactor)
        sut.parseInput("some input")
        
        XCTAssertEqual(sut.parsedInput(), "No results")
    }
}

class ChatMessageFakeInteractor: ChatMessageInteractorProtocol {
    
    var modelDidUpdate: ((String) -> ())?
    
    var model: String = ""
    
    var emptyParse = false
    
    func parseInput(_ string: String) {
        self.model = emptyParse ? "" : "foo bar"
        self.modelDidUpdate?(self.model)
    }
}
