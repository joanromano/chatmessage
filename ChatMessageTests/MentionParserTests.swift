//
//  MentionParserTests.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import XCTest

class MentionParserTests: XCTestCase {
    
    func testParserOnlyMentions() {
        let sut = MentionParser()
        let result = sut.parse("@joan @andy")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@andy")
    }
    
    func testParserMentionsWithText() {
        let sut = MentionParser()
        let result = sut.parse("asdsad @joan sadsadsad asdsadsadsa @andy")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@andy")
    }
    
    func testParseNoMentions() {
        let sut = MentionParser()
        let result = sut.parse("asdsad (joan) sadsadsad asdsadsadsa andy")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testParseEmptyText() {
        let sut = MentionParser()
        let result = sut.parse("")
        
        XCTAssertEqual(result.count, 0)
    }
    
    func testParserMentionsWithTextUppercase() {
        let sut = MentionParser()
        let result = sut.parse("asdsad @joan sadsadsad asdsadsadsa @ANDY")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@ANDY")
    }
    
    func testEmptyMentions() {
        let sut = MentionParser()
        let result = sut.parse("asdsad @ sadsadsad asdsadsadsa @andy")
        
        XCTAssertEqual(result.count, 1)
        XCTAssertEqual(result.first as! String, "@andy")
        XCTAssertEqual(result.last as! String, "@andy")
    }
    
    func testMentionInsideMention() {
        let sut = MentionParser()
        let result = sut.parse("asdsad @joan@romano sadsadsad asdsadsadsa @andy @manuel@pils@whatever")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@manuel")
    }
    
    func testMentionBeginningAndEnd() {
        let sut = MentionParser()
        let result = sut.parse("@joan@romano sadsadsad asdsadsadsa @andy")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@andy")
    }
    
    func testMentionWithNumbers() {
        let sut = MentionParser()
        let result = sut.parse("@joan@romano sadsadsad asdsadsadsa @andy123")
        
        XCTAssertEqual(result.count, 2)
        XCTAssertEqual(result.first as! String, "@joan")
        XCTAssertEqual(result.last as! String, "@andy123")
    }
    
    func testMentionWithUnderscores() {
        let sut = MentionParser()
        let result = sut.parse("@_romano_ sadsadsad @joan asdsadsadsa @andy123")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "@_romano_")
        XCTAssertEqual(result.last as! String, "@andy123")
    }
    
    func testMentionWithNoWordCharacters() {
        let sut = MentionParser()
        let result = sut.parse("@_romano- sadsadsad @1jo%an asdsadsadsa @and$y123")
        
        XCTAssertEqual(result.count, 3)
        XCTAssertEqual(result.first as! String, "@_romano")
        XCTAssertEqual(result.last as! String, "@and")
    }
}
