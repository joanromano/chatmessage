//
//  AppDelegate.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        let parser = JSONParser(parsers: [NonAlphanumericParser(MentionParser()), NonAlphanumericParser(EmoticonParser()), LinkParser()])
        let interactor = ChatMessageInteractor(parser: parser)
        let vc = ChatMessageViewController(binding: interactor) { (interactor, model) -> ChatMessageViewModel in
            return ChatMessageViewModel(interactor: interactor)
        }
        
        window?.rootViewController = UINavigationController(rootViewController: vc)
        
        return true
    }


}

