//
//  WrapperParsers.swift
//  ChatMessage
//
//  Created by Joan Romano on 07/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

import MWFeedParser

/// A Parseable wrapper that wrapps another parseable and removes non alphanumeric characters from it
struct NonAlphanumericParser: Parseable {
    /// The wrapped parser
    let parser: Parseable
    
    var title: String { return parser.title }
    
    init(_ parser: Parseable) {
        self.parser = parser
    }
    
    func parse(_ string: String) -> [Any] {
        let characterSet = CharacterSet.alphanumerics.inverted
        return parser.parse(string).filter { $0 is String }.flatMap { ($0 as! String).components(separatedBy: characterSet).joined(separator: "") }
    }
}

/// A Parseable wrapper that wrapps another parseable and decodes HTML characters from it
struct HTMLEscapedParser: Parseable {
    /// The wrapped parser
    let parser: Parseable
    
    var title: String { return parser.title }
    
    init(_ parser: Parseable) {
        self.parser = parser
    }
    
    func parse(_ string: String) -> [Any] {
        return parser.parse(string).filter { $0 is String }.flatMap { ($0 as! NSString).decodingHTMLEntities() }
    }
}

/// Parseable struct that uses URLParser and URLTitleParser internally to provide full link parsing
struct LinkParser: Parseable {
    /// The URLParser used
    let urlParser = URLParser()
    
    /// The URLTitleParser used
    let urlTitleParser = HTMLEscapedParser(URLTitleParser())
    
    var title = "links"
    
    /// Synchronously parses links using URLParser and URLTitleParser
    ///
    /// - Parameter string: The input string
    /// - Returns: An array of matched dictionaries as Any, with the following representation per dictionary:
    ///
    /// { "url": "https://exampleURL", "title": "HTML title" }
    func parse(_ string: String) -> [Any] {
        let urlStrings = urlParser.expresion.matches(in: string, options: [], range: validRange(for: string)).flatMap { $0.url?.absoluteString }
        let urlTitleStringInputClosure: (String) -> (String) = { try! NSString(contentsOf: URL(string: $0)!, encoding: String.Encoding.ascii.rawValue) as String } // URLs should be valid at this point as result from URLParser, thus fair to force unwrap/try
        var links = [[String:String]]()
        urlStrings.forEach {
            links.append([urlParser.title: $0,
                          urlTitleParser.title: urlTitleParser.parse(urlTitleStringInputClosure($0)).first as? String ?? ""])
        }
        
        return links
    }
}

/// A struct that parses a string using a given array of Parseable parsers into a JSON string
struct JSONParser {
    /// The array of parsers to be used
    let parsers: [Parseable]
    
    /// Parses the given string using an array of parsers, aggregating the result in a json string
    ///
    /// - Parameter string: The input string
    /// - Returns: A json string with the aggregated result of all parsers, with the following structure:
    ///
    /// { "parser_title": "parser_result" }
    func parse(_ string: String) -> String {
        var resultDict = [String: Any]()
        var resultJSONString = ""
        
        for parser in parsers {
            let parsed = parser.parse(string)
            if parsed.count > 0 {
                resultDict[parser.title] = parsed
            }
        }
        
        if JSONSerialization.isValidJSONObject(resultDict) {
            // From JSONSerialization docs, assume that `isValidJSONObject` let us force try the conversion
            let jsonData = try! JSONSerialization.data(withJSONObject: resultDict, options: .prettyPrinted)
            if let jsonString = String(data: jsonData, encoding: String.Encoding.utf8),
                   jsonString != "{\n\n}" {
                resultJSONString = jsonString.replacingOccurrences(of: "\\", with: "")
            }
        }
        
        return resultJSONString
    }
}
