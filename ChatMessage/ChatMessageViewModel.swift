//
//  ChatMessageViewModel.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import UIKit

/// A view model to encapsulate presentation logic for the view layer
class ChatMessageViewModel {

    let interactor: ChatMessageInteractorProtocol
    
    /// Initializes a new ChatMessageViewModel
    ///
    /// - Parameter interactor: The ChatMessageInteractor to be used
    init(interactor: ChatMessageInteractorProtocol) {
        self.interactor = interactor
    }
    
    /// Forwards the parsing to the interactor
    ///
    /// - Parameter string: the input string
    func parseInput(_ string: String) {
        interactor.parseInput(string)
    }
    
    /// The current parsed input, formatted for the view layer
    ///
    /// - Returns: A string with the formatted parsed input, or "No results" if there is no valid formatted input
    func parsedInput() -> String {
        return interactor.model.characters.count > 0 ? interactor.model : "No results"
    }
    
}
