//
//  Parsers.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

/// RegularExpressionPatternParseable struct to parse mentions
/// 
/// Matches mentions starting with an '@' and ending when hitting a non-word character.
struct MentionParser: RegularExpressionPatternParseable {
    var pattern = "\\B@[\\w]+"
    var title = "mentions"
}

/// RegularExpressionPatternParseable struct to parse emoticons
///
/// Matches emoticons which are alphanumeric strings, no longer than 15 characters, contained in parenthesis.
struct EmoticonParser: RegularExpressionPatternParseable {
    var pattern = "\\(([\\w]{1,15}?)\\)"
    var title = "emoticons"
}

/// RegularExpressionPatternParseable struct to parse title tag content inside html strings
struct URLTitleParser: RegularExpressionPatternParseable {
    var pattern = "(?<=<title>)(.*)(?=</title>)"
    var title = "title"
}

/// RegularExpressionParseable struct to parse URLs
struct URLParser: RegularExpressionParseable {
    var expresion: NSRegularExpression = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
    var title = "url"
}
