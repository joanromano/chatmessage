//
//  ChatMessageViewController.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import UIKit

class ChatMessageViewController: UIViewController, UpdateableView {
    
    var viewModel: ChatMessageViewModel {
        didSet {
            updateSubviews()
        }
    }
    
    let containerSrollView = UIScrollView()
    let inputTextView = UITextView()
    let resultLabel = UILabel()
    
    required init(viewModel: ChatMessageViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UIViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSubviews()
        updateSubviews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        inputTextView.becomeFirstResponder()
    }
    
    // MARK: - Actions
    
    func goPressed() {
        toggleNavigationButtonItem()
        viewModel.parseInput(inputTextView.text)
    }
    
    // MARK: - Private
    
    func setupSubviews() {
        navigationItem.title = "Message Parser"
        view.backgroundColor = .white
        inputTextView.backgroundColor = .lightGray
        inputTextView.font = UIFont.systemFont(ofSize: 14)
        resultLabel.font = UIFont.systemFont(ofSize: 14)
        resultLabel.numberOfLines = 0
        containerSrollView.translatesAutoresizingMaskIntoConstraints = false
        inputTextView.translatesAutoresizingMaskIntoConstraints = false
        resultLabel.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(containerSrollView)
        containerSrollView.addSubview(inputTextView)
        containerSrollView.addSubview(resultLabel)
        
        setupContraints()
    }
    
    func setupContraints() {
        let margin: CGFloat = 20
        let textViewHeight: CGFloat = 150
        
        NSLayoutConstraint(item: containerSrollView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: containerSrollView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: containerSrollView, attribute: .leading, relatedBy: .equal, toItem: view, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: containerSrollView, attribute: .trailing, relatedBy: .equal, toItem: view, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        
        NSLayoutConstraint(item: inputTextView, attribute: .top, relatedBy: .equal, toItem: containerSrollView, attribute: .top, multiplier: 1, constant: margin).isActive = true
        NSLayoutConstraint(item: inputTextView, attribute: .centerX, relatedBy: .equal, toItem: containerSrollView, attribute: .centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: inputTextView, attribute: .width, relatedBy: .equal, toItem: containerSrollView, attribute: .width, multiplier: 1, constant: -margin * 2).isActive = true
        NSLayoutConstraint(item: inputTextView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: textViewHeight).isActive = true
        
        NSLayoutConstraint(item: resultLabel, attribute: .top, relatedBy: .equal, toItem: inputTextView, attribute: .bottom, multiplier: 1, constant: margin).isActive = true
        NSLayoutConstraint(item: resultLabel, attribute: .bottom, relatedBy: .lessThanOrEqual, toItem: containerSrollView, attribute: .bottom, multiplier: 1, constant: -margin).isActive = true
        NSLayoutConstraint(item: resultLabel, attribute: .leading, relatedBy: .equal, toItem: inputTextView, attribute: .leading, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: resultLabel, attribute: .trailing, relatedBy: .equal, toItem: inputTextView, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
    }
    
    func updateSubviews() {
        toggleNavigationButtonItem()
        resultLabel.text = viewModel.parsedInput()
    }
    
    func toggleNavigationButtonItem() {
        if let _ = navigationItem.rightBarButtonItem?.title {
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            activityIndicator.startAnimating()
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: activityIndicator)
        } else {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Go", style: .plain, target: self, action: #selector(goPressed))
        }
    }
}

