//
//  Parseable.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

/// A parser for input strings
protocol Parseable {
    /// A title to use for the parsed string
    var title: String { get }
    
    /// Parses the input string
    ///
    /// - Parameter string: The input string
    /// - Returns: An array of Any
    func parse(_ string: String) -> [Any]
    
    /// The range to be used when parsing the string, default to the full range of the string
    ///
    /// - Parameter string: The input string
    /// - Returns: The range to use when parsing the string
    func validRange(for string: String) -> NSRange
}

extension Parseable {
    func validRange(for string: String) -> NSRange {
        return NSRange(location: 0, length: string.characters.count)
    }
}

/// A Parseable which by default parses input strings using given NSRegularExpressions
protocol RegularExpressionParseable: Parseable {
    /// A given NSRegularExpression to be used for parsing
    var expresion: NSRegularExpression { get }
}

extension RegularExpressionParseable {
    /// Default implementation that synchronously returns the matches for the expression in a given input string
    ///
    /// - Parameter string: The input string
    /// - Returns: Array of string matches as Any
    func parse(_ string: String) -> [Any] {
        return expresion.matches(in: string, options: [], range: validRange(for: string)).flatMap {
            return (string as NSString).substring(with: $0.range)
        }
    }
}

/// A RegularExpressionParseable which by default parses input strings using NSRegularExpressions with a given pattern string
protocol RegularExpressionPatternParseable: RegularExpressionParseable {
    /// The pattern to be used in `expresion`
    var pattern: String { get }
}

extension RegularExpressionPatternParseable {
    var expresion: NSRegularExpression {
        do {
            return try NSRegularExpression(pattern: pattern, options: [])
        } catch {
            fatalError("Invalid pattern or expression for RegularExpressionPatternParseable")
        }
    }
}
