//
//  Interactor.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import Foundation

/// A protocol for declaring Interactors, which are responsible for managing and updating the `Model` (i.e *CRUD* operations).
public protocol Interactor: class {
    /// The associated type of the model
    associatedtype ModelType
    
    /// The model
    var model: ModelType { get }
    
    /// A closure which gets executed when the model gets updated
    var modelDidUpdate: ((ModelType) -> Void)? { get set }
}
