//
//  ChatMessageInteractor.swift
//  ChatMessage
//
//  Created by Joan Romano on 06/11/16.
//  Copyright © 2016 Atlassian. All rights reserved.
//

import UIKit

/// A protocol to represent a ChatMessageInteractor, mainly for testing purposes
protocol ChatMessageInteractorProtocol {
    var modelDidUpdate: ((String) -> ())? { get set }
    var model: String { get }
    
    func parseInput(_ string: String)
}

/// An interactor in charge of parsing the input using parsers
class ChatMessageInteractor: ChatMessageInteractorProtocol, Interactor {

    var modelDidUpdate: ((String) -> ())?
    
    var model: String = ""
    
    let parser: JSONParser
    
    /// Initializes a new ChatMessageInteractor
    ///
    /// - Parameter parser: a JSONParser to be used in order to parse input strings
    init(parser: JSONParser) {
        self.parser = parser
    }

    /// Asnchronously forwards the parsing to the parser, and updates the model accordingly when parsing is finished
    ///
    /// - Parameter string: the input string
    func parseInput(_ string: String) {
        DispatchQueue.global(qos: .userInitiated).async {
            let newModel = self.parser.parse(string)
            
            DispatchQueue.main.async {
                self.model = newModel
                self.modelDidUpdate?(self.model)
            }
        }
    }
}
